import React, { useState, useEffect } from "react";
import Service from "../../services/serviceManager";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import Dialog from "@material-ui/core/Dialog";
import Button from "@material-ui/core/Button";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";

import "./Home.scss";

import grapevid from "../../assets/grapevid.svg";
import virus from "../../assets/virus.svg";

import Detail from "../Detail/Detail";
import LineChart from "../Charts/LineChart";
import LineChart2 from "../Charts/LineChart2";
import LineChart3 from "../Charts/LineChart3";
import BarChart from "../Charts/BarChart";

import styles from "../StyleMaterial/StyleMaterial";

const Home = () => {
  const classes = styles();

  const [home, setHome] = useState({
    countryName: [],
    searchCountry: "Colombia",
  });

  useEffect(() => {
    Service("all").then((res) => {
      setHome({ ...home, countryName: res.data });
    });
  }, []);

  function isMobile() {
    return (
      navigator.userAgent.match(/Android/i) ||
      navigator.userAgent.match(/webOS/i) ||
      navigator.userAgent.match(/iPhone/i) ||
      navigator.userAgent.match(/iPod/i) ||
      navigator.userAgent.match(/iPad/i) ||
      navigator.userAgent.match(/BlackBerry/i)
    );
  }

  const [open, setOpen] = React.useState(isMobile());

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div className="home">
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Para mejorar la visualizacion de los datos te recomendamos girar tu
            dispositivo movil.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Acepto
          </Button>
        </DialogActions>
      </Dialog>

      <div id="NavBar">
        <img src={grapevid} className="Logo" />
        <h1>&nbsp;GrapeVid&nbsp;</h1>
      </div>

      <div id="NavBar2">
        <h2>&nbsp;Reporte de análisis demográfico para&nbsp;</h2>
        <img src={virus} className="Logo" />
        <h2>&nbsp;COVID-19 *&nbsp;</h2>
      </div>

      <h3> Por tú salud y la de todos #QuedateEnCasa </h3>

      <div
        style={{
          height: 150,
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <h2>Filtrar por pais</h2>

        <Select
          className={classes.comboBox}
          defaultValue="Colombia"
          onChange={(value) =>
            setHome({ ...home, searchCountry: value.target.value })
          }
        >
          {home.countryName.map((item) => (
            <MenuItem value={item.country}> {item.country} </MenuItem>
          ))}
        </Select>
      </div>

      <div id="CntInfo">
        <Detail countryName={home.searchCountry} />
      </div>

      <div id="GnrContLine">
        <div id="CtnLineGraph">
          <h2 style={{ color: "black" }}> Curva de Contagio Diario </h2>
          <p>Evolución de casos diarios durante los ultimos 30 dias</p>
          <LineChart3 countryName={home.searchCountry} />
        </div>
      </div>

      <div id="GnrContLine">
        <div id="CtnLineGraph">
          <h2 style={{ color: "black" }}> Curva de Contagio Total </h2>
          <p>Evolución de casos totales durante los ultimos 30 dias</p>
          <LineChart2 countryName={home.searchCountry}/>
        </div>
      </div>

      <div id="GnrContLine">
        <div id="CtnLineGraph">
          <LineChart countryList={home.countryName} />
        </div>
      </div>

      <div id="GnrContLine">
        <div id="CtnLineGraph">
          <h2 style={{ color: "black" }}> Información Global </h2>
          <p>Muestra datos del COVID-19 a nivel global</p>
          <BarChart />
        </div>
      </div>

      <div id="GnrContLine">
        <div id="CtnLineGraph">
          <h2 style={{ color: "black" }}> Notas </h2>
          <p>
            * Ningún análisis esta avaluado por expertos epidemiólogos, son
            resúmenes estadísticos y modelos de calculo aproximado. Si bien son
            datos reales de dominio publico entregados por la OMS, y otras
            instituciones, no están exentos de desestimación de parámetros que
            afecten los reportes.
          </p>
          <p>
            ** Estos modelos no representan un valor fijo del comportamiento del
            fenómeno, son aproximaciones dadas características continuas y
            valores iniciales diarios de referencia.
          </p>
        </div>
      </div>

      <div id="GnrContLine">
        <div id="CtnLineGraph">
          <p>
            Copyright &#169; 2020<b> InGrapes S.A.S. </b>Todos los derechos
            reservados.{" "}
          </p>
        </div>
      </div>
    </div>
  );
};

export default Home;

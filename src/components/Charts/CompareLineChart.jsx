import React, { useState, useEffect } from "react";
import Service from "../../services/serviceManager";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import { Bar } from "react-chartjs-2";
import "./CompareLineChart.scss";

const CompareLine = () => {
  const [compare, setCompare] = useState({
    countryNames: [],
    comparisson: {
      labels: [],
      datasets: []
    },
    compare1: "Colombia",
    compare2: "Venezuela"
  });

  const renderMenuItem = () => {
    return compare.countryNames.map(item => {
      return <MenuItem value={item.name}> {item.name} </MenuItem>;
    });
  };

  const getComparison = () => {
    const request = {
      countries: [
        {
          country: compare.compare1
        },
        {
          country: compare.compare2
        }
      ]
    };

    Service("compare", "POST", request).then(res => {
      setCompare({
        ...compare,
        comparisson: {
          labels: [compare.compare1, compare.compare2],
          datasets: [
            {
              label: "Reportados",
              data: [
                res.data[0].dataCountry.cases,
                res.data[1].dataCountry.cases
              ],
              backgroundColor: ["#F7DC6F", "#F7DC6F"]
            },
            {
              label: "Recuperados",
              data: [
                res.data[0].dataCountry.recovered,
                res.data[1].dataCountry.recovered
              ],
              backgroundColor: ["#76D7C4", "#76D7C4"]
            },
            {
              label: "Criticos",
              data: [
                res.data[0].dataCountry.critical,
                res.data[1].dataCountry.critical
              ],
              backgroundColor: ["#F0B27A", "#F0B27A"]
            },
            {
              label: "Decesos",
              data: [
                res.data[0].dataCountry.deaths,
                res.data[1].dataCountry.deaths
              ],
              backgroundColor: ["#D98880", "#D98880"]
            }
          ]
        }
      });
    });
  };

  useEffect(() => {
    Service("countryList", "GET").then(res => {
      setCompare({ ...compare, countryNames: res.data });
    });
  }, []);

  return (
    <div>
      <div>
        <Select
          className="select"
          onChange={value =>
            setCompare({ ...compare, compare1: value.target.value })
          }
        >
          {renderMenuItem()}
        </Select>

        <Select
          className="select"
          onChange={value =>
            setCompare({ ...compare, compare2: value.target.value })
          }
        >
          {renderMenuItem()}
        </Select>
      </div>

      <button onClick={() => getComparison()} id='BtnCompare'> Comparar </button>
      
      <Bar data={compare.comparisson} options={{
        plugins: {
          datalabels: {
            display: true,
            color: "black"
          }
        }
      }} />
    </div>
  );
};

export default CompareLine;

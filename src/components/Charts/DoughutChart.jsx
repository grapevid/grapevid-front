import React from "react";
import { Doughnut } from "react-chartjs-2";

const Doughut = props => {
  return (
    <Doughnut
      data={props.chartData}
      options={{
        responsive: true,
        maintainAspectRatio: true,
        width: "500",
        height: "500",
        plugins: {
          datalabels: {
            display: true,
            color: "black"
          }
        }
      }}
    />
  );
};
export default Doughut;

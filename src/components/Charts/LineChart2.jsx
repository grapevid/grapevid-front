import React, { useState, useEffect } from "react";
import { Line } from "react-chartjs-2";
import Select from "@material-ui/core/Select";
import Service from "../../services/otherServices";

import "./LineChart.scss";

const LineChart2 = (props) => {
  const [data, setdata] = useState();

  useEffect(() => {
    Service(
      "https://api.covid19api.com/total/country/" + props.countryName,
      "GET"
    ).then((res) => {
      let datalabel = [];
      let dataset1 = [];
      let dataset2 = [];
      let dataset3 = [];
      for (
        let i = Object.keys(res.data).length - 30;
        i < Object.keys(res.data).length;
        i++
      ) {
        datalabel.push(JSON.stringify(res.data[i].Date).slice(1, 11));
        dataset1.push(JSON.stringify(res.data[i].Confirmed));
        dataset2.push(JSON.stringify(res.data[i].Recovered));
        dataset3.push(JSON.stringify(res.data[i].Deaths));
      }
      setdata({
        labels: datalabel,
        datasets: [
          {
            label: "Reportados",
            data: dataset1,
            borderColor: "#F7DC6F",
            backgroundColor: "transparent",
          },
          {
            label: "Recuperados",
            data: dataset2,
            borderColor: "#76D7C4",
            backgroundColor: "transparent",
          },
          {
            label: "Decesos",
            data: dataset3,
            borderColor: "#D98880",
            backgroundColor: "transparent",
          },
        ],
      });
    });
  }, [props.countryName]);

  return (
    <div className="graf">
      <Line
        data={data}
        options={{
          plugins: {
            datalabels: {
              display: false,
              color: "black",
            },
          },
        }}
      />
    </div>
  );
};

export default LineChart2;

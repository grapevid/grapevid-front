import React, { useState, useEffect } from "react";
import Service from "../../services/otherServices";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import { Bar } from "react-chartjs-2";
import "./CompareLineChart.scss";
import "chartjs-plugin-datalabels";

const BarLine = () => {

  const [data, setdata] = useState(null);

  useEffect(()=>{
    Service("https://api.covid19api.com/summary", "GET").then(res => {
      setdata({
        labels: ["Totales","Nuevos"],
        datasets: [
          {
            label: "Reportes",
            data: [res.data.Global.TotalConfirmed,
              res.data.Global.NewConfirmed],
            backgroundColor: ["#F7DC6F","#F7DC6F"]
          },
          {
            label: "Recuperados",
            data: [res.data.Global.TotalRecovered,
              res.data.Global.NewRecovered],
            backgroundColor: ["#76D7C4","#76D7C4"]
          },
          {
            label: "Decesos",
            data: [res.data.Global.TotalDeaths,
              res.data.Global.NewDeaths],
            backgroundColor: ["#D98880","#D98880"]
          }
        ]
      });
    });
  },[])
    
  return (
    <div>
      <Bar data={data} options={{
        plugins: {
          datalabels: {
            display: true,
            color: "black"
          }
        }
      }} />
    </div>
  );
};

export default BarLine;

import React, { useState, useEffect } from "react";
import { Line } from "react-chartjs-2";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import Service from "../../services/serviceManager";
import { relativeTimeRounding } from "moment";
import { MenuList } from "@material-ui/core";

import "./LineChart.scss";


const LineChart = props => {
  const [dataManager, setDataManager] = useState();

  const [selectManager, setSelectManager] = useState({
    countries: [],
    modelMath: [],
    selectedCountry: "Colombia",
    titleMethod: "Anderson May",
    selectedMethod: "andersonmay"
  });

  const loadmethoddata = async () =>{
    let label = [];
    let tmpDataSets = [];
    let i = 0;
    let colores = ["#F7DC6F","#76D7C4","#F0B27A","#D98880"];
    await Service(
      `${selectManager.selectedMethod}/${selectManager.selectedCountry}`,
      "GET"
    ).then(res => {
      Object.entries(res).forEach(([prop, val]) => {
        i++;
        if (i === 1) {
          label = JSON.parse(val);
        } 
        else {
          tmpDataSets.push({
            backgroundColor: "transparent",
            borderColor: colores[i-2],
            label: prop,
            data: JSON.parse(val)
          });
        }
      });
    });

    setTimeout(
      () => setDataManager({ labels: label, datasets: tmpDataSets }),
      500
    );
  };


  useEffect(() => {
    loadmethoddata();
  }, [selectManager.selectedCountry, selectManager.selectedMethod]);

  useEffect(() => {
    Service("methodlist", "GET").then(res => {
      setSelectManager({ ...selectManager, modelMath: res.data });
    });
  }, []);

  const renderListCountry = () => {
    return props.countryList.map(result => {
      return <MenuList value={result.country}> {result.country}</MenuList>;
    });
  };

  const renderMethodList = () => {
    return selectManager.modelMath.map(result => {
      console.log(result);
      return <MenuList value={result}> {result.titleName} </MenuList>;
    });
  };


  return (
    <div>
      <h2 style={{color:'black'}}> Modelos Epidemiológicos </h2>
      <p>Comportamiento poblacional del COVID-19 basado en modelos epidemiológicos **</p>
      <div className="selects">
        <Select
          defaultValue={selectManager.selectedCountry}
          className="select1"
          onChange={value =>
            setSelectManager({
              ...selectManager,
              selectedCountry: value.target.value
            })
          }
        >
          {renderListCountry()}
        </Select>

        <Select
          className="select1"
          onChange={value =>
            setSelectManager({
              ...selectManager,
              titleMethod: value.target.value.titleName,
              selectedMethod: value.target.value.methodPath
            })
          }
        >
          {renderMethodList()}
        </Select>
      </div>
      <Line data={dataManager} options={{
        plugins: {
          datalabels: {
            display: false
          }
        }
      }} />
    </div>
  );
};

export default LineChart;

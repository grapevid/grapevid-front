import { makeStyles } from '@material-ui/core/styles';


export default  makeStyles ({
  comboBox: {
      width:'40%',
      height:'50px',
      backgroundColor:'#b03b8b3d',
      fontSize:20,
      fontWeight:'600',
      fontStyle:'italic',
  },

  comboBoxCompare:{

  },
  GnrlCntItems: {
    width:'100%',
    display:'flex',
    flexDirection:'column',
    backgroundColor:'white',
    marginTop:'1.5%',
    alignItems:'center'
  },


  CntDataLeft:{
    width:'20%'
  },
  CntDataRight:{
    width:'20%'
  },
  CtnTopItems:{
    width:'100%',
    display:'flex',
    flexDirection:'row',
    justifyContent:'space-around',
  },
  CtnDona: {
    width:'100%',
  },

});

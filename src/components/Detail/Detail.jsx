import React, { useState, useEffect } from "react";
import Service from "../../services/serviceManager";
import "moment/locale/es";

import CompareLineChart from '../Charts/CompareLineChart'
import DoughutChart from '../Charts/DoughutChart'

import './Detail.scss'
import styles from '../StyleMaterial/StyleMaterial'

const Detail = props => {

  const classes = styles();


  const [detail, setDetail] = useState({
    countryName: "Colombia",
    countryList: [],
    isLoading: false,
    countryInformation: {},
    flag: "",
    chartData: {}
  });

  useEffect(() => {
    Service("getCountryByName/" + props.countryName, "GET").then(res => {
      setDetail({
        ...detail,
        countryInformation: res.data,
        flag: res.data.countryInfo.flag,
        chartData: {
          labels: ["Reportados", "Recuperados", "Criticos", "Decesos"],
          datasets: [
            {
              data: [res.data.cases, res.data.recovered, res.data.critical, res.data.deaths],
              backgroundColor: ["#F7DC6F", "#76D7C4", "#F0B27A", "#D98880"]
            }
          ]
        }
      });
    });
  }, [props.countryName]);
  return (

    <div className={classes.GnrlCntItems}>

      <div className={classes.CtnTopItems}>

        <div className={classes.CntDataLeft}>
          <h3> Reportados </h3>
          <h2 className="nogood"> {detail.countryInformation.cases} </h2>
          <h3> Recuperados </h3>
          <h2 className="good"> {detail.countryInformation.recovered} </h2>
          {/**
          <h3> Total infectados {Moment(Date.now()).format("LLL")}: </h3>
          <h2> {detail.countryInformation.todayCases} </h2> */}
        </div>

        <div id='CtnFlag' >
          <img src={detail.flag} alt="" style={{width:'80%'}}/>
        </div>
 
        <div className={classes.CntDataRight}>
          <h3> Críticos </h3>
          <h2 className="bad"> {detail.countryInformation.critical} </h2>
          <h3> Decesos </h3>
          <h2 className="fuck"> {detail.countryInformation.deaths} </h2>
          {/**
          <h3> Total muertos {Moment(Date.now()).format("LLL")}: </h3>
          <h2> {detail.countryInformation.todayDeaths} </h2> */}
        </div>

      </div>

      <div id='Cont2Graphs'>

        <div id='CtnCompare'>
          <h2 style={{color:'black'}}> Gráfico Comparativo </h2>
            <p>Compara datos del COVID-19 entre diferentes paises</p>
          <CompareLineChart/>
        </div>

        <div id='CtnDona'>
          <h2 style={{color:'black'}}> Gráfico Circular </h2>
            <p>Gráfico estadístico de datos generales</p>
          <DoughutChart chartData = {detail.chartData}/>
        </div>

      </div>
    </div>
  );
};

export default Detail;

import Axios from "axios";

const URL = "https://grapevid-api.herokuapp.com/api/";
//const URL = "https://localhost:5000/api/"
export default async (path, method, data) => {
  try {
    return await Axios({
      url: URL + path,
      method: method,
      data: data,
    })
    .then(response => {
        return response.status === 200 ? response.data : 'ERROR';
      })
      .catch(error => {
        console.log(error)
        alert(error);
      });
  } catch (err) {
    console.log(err);
  }
};
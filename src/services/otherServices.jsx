import Axios from "axios";

export default async (url, method, data) => {
  try {
    return await Axios({
      url,
      method,
      data,
    })
      .then((response) => {
        return response;
      })
      .catch((error) => {
        console.log(error);
      });
  } catch (err) {
    console.log(err);
  }
};
